import timeit

def sort():
    start_time = timeit.default_timer()
    numbers = None
    with open('to_be_sorted.txt', 'r') as f:
        lines = f.readlines()
        numbers = list(map(lambda x: int(x), lines))
    
    # Bucket sorting with len(numbers) buckets
    sorted = [None] * len(numbers)
    for number in numbers:
        sorted[number] = str(number)

    with open('sorted.txt', 'w') as f:
        f.write('\n'.join(sorted))
    elapsed = timeit.default_timer() - start_time
    print(elapsed)


if __name__ == '__main__':
    sort()
