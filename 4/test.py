import reverse_engineer_fn as reverse_engineer_fn

string = "Hello my"

def fnn(string):
    message_fr = "Brute force c'est pour la pleb"
    message_en = 'Brute force is for the weak'
    new_string = ''
    for c in string:
        if c in message_fr:
            new_string = new_string + c
        else:
            new_string = c + new_string

    bytes = new_string.encode('utf-8')
    bytes_result = bytearray()
    for byte in bytes:
        bytes_result.append(byte if byte < 105 else byte - 7)

    return ('').join(bytes_result.decode('utf-8'))
 
print('given function -> ', reverse_engineer_fn.fn(string))
print('my function -> ', fnn(string))