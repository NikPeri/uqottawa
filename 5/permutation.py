import json
import itertools
from flask import Flask
from flask import request, Response

app = Flask(__name__)

@app.route('/permutations', methods=['POST'])
def permutations():
    numbers = list(map(lambda x: str(x), request.form['input'].split(' ')))
    result = list(itertools.permutations(numbers))
    
    return Response(json.dumps({
        'result': list(map(lambda x: ' '.join(list(x)), result))
    }), mimetype=u'application/json') 
    
if __name__ == "__main__":
    app.run()
